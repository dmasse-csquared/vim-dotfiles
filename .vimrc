runtime bundle/vim-pathogen/autoload/pathogen.vim
execute pathogen#infect()

" Enable syntax highlighting
syntax on

filetype plugin indent on

" Airline color scheme
let g:airline_theme = 'dark'
let g:airline_powerline_fonts = 1
if !exists('g:airline_symbols')
  let g:airline_symbols = {}
endif
" powerline symbols
let g:airline_left_sep = ''
let g:airline_left_alt_sep = ''
let g:airline_right_sep = ''
let g:airline_right_alt_sep = ''
let g:airline_symbols.branch = ''
let g:airline_symbols.readonly = ''
let g:airline_symbols.linenr = ''
let g:airline_symbols.notexists = ''
let g:airline_symbols.dirty=''

let g:bookmark_sign = ''
let g:bookmark_highlight_lines = 1

" General VIM color scheme
colorscheme mustang

" Automatically indent based on syntax
set autoindent
" Allow delete key on Mac to work like backspace
set backspace=2
" Prevent saving twice and causing grunt to run a second time
set backupcopy=yes
set backupdir=$HOME/.vim/tmp
" Use the indent from the previous line
set copyindent
set directory=$HOME/.vim/tmp
set expandtab
set fileencoding=utf8
" Highlight matching search terms
set hlsearch
" Ignore case in searches
set ignorecase
" Start searching right away while typing
set incsearch
" Allow airline to display properly
set laststatus=2
" Show line numbers
set number
" Let F2 toggle paste functionality
set pastetoggle=<F2>
set shiftwidth=4
" Open new document below when splitting horizontally
set splitbelow
" Open new document to the right when splitting vertically
set splitright
set tabstop=4
set timeoutlen=1000 ttimeoutlen=0

" Highlight current line
highlight CursorLine cterm=NONE ctermbg=237 ctermfg=NONE guibg=white guifg=white
set cursorline

" Disable Markdown folding
let g:vim_markdown_folding_disabled=1

" Python-mode
let g:pymode_folding = 0
let g:pymode_lint = 1
let g:pymode_lint_checkers = ['pep8']
let g:pymode_lint_ignore = "E501"
let g:pymode_lint_message = 1
let g:pymode_lint_on_write = 1
autocmd FileType python let g:syntastic_check_on_wq = 0

let g:syntastic_php_checkers=['php']
let g:syntastic_javascript_checkers = ['standard']
let g:syntastic_javascript_standard_exec = 'happiness'
let g:syntastic_javascript_standard_generic = 1

" Cygwin cursor fixes
let &t_ti.="\e[1 q"
let &t_SI.="\e[5 q"
let &t_EI.="\e[1 q"
let &t_te.="\e[0 q"

" Key mappings
nnoremap <C-j> m`o<Esc>``
nnoremap <C-k> m`O<Esc>``

:command! Space set shiftwidth=4 tabstop=4 expandtab
:command! Space2 set shiftwidth=2 tabstop=2 expandtab
:command! Tab set shiftwidth=4 tabstop=4 noexpandtab

" File-specific settings
autocmd BufNewFile,BufRead *.php set syntax=php
autocmd BufNewFile,BufRead *.js set syntax=javascript
autocmd BufNewFile,BufRead Vagrantfile set filetype=ruby
autocmd BufNewFile,BufRead *.yaml,*.yml so ~/.vim/colors/yaml.vim

" Trim long filenames before adding to tmux window
function TrimTitle()
    if strlen(expand("%:t")) > 20
        call system("tmux rename-window " . strpart(expand("%:t"), 0, 20) . "...")
    else
        call system("tmux rename-window " . expand("%:t"))
    endif
endfunction

if exists('$TMUX')
    autocmd BufNewFile,BufReadPost,FileReadPost,WinEnter * call TrimTitle()
    autocmd VimLeave * call system("tmux rename-window bash")
endif

" VDebug
let g:vdebug_options = {'break_on_open': 0}
let g:vdebug_options = {'server': 'localhost'}
let g:vdebug_options = {'port': '9009'}
